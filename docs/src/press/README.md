---
sidebar: auto
---

# Meltano Press

Meltano related news, logos, and how to get in touch.

## Top Stories

- [GitLab to create tool for data teams](https://sdtimes.com/data/gitlab-to-create-tool-for-data-teams/)
- [Hey, data teams - We're working on a tool just for you](https://about.gitlab.com/2018/08/01/hey-data-teams-we-are-working-on-a-tool-just-for-you/)
- [Hacker News discussion](https://news.ycombinator.com/item?id=17667399)

## Logos

<LogoList />

## Get in Touch

For press inquiries, please email [hello@meltano.com](mailto:hello@meltano.com).

You can also follow us on: 

- [YouTube](https://www.youtube.com/meltano)
- [Twitter](https://twitter.com/meltanodata)
- [Slack](https://join.slack.com/t/meltano/shared_invite/enQtNTM2NjEzNDY2MDgyLTZhY2QzYzkwNjYzNWY5Zjk5ZTE1ZGExNzE1NTFmMWJiM2E2ODVhMDFlYjc5YzVjMjllZTZlZDVjNWU2ZjNjNzQ)
