# Plugins

Meltano's ecosystem consist of plugins to help you facilitate your data pipeline. You can learn more about our:

- [Extractors](/plugins/extractors/)
- [Loaders](/plugins/Loaders/)
